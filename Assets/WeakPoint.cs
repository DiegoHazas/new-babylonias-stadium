using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WeakPoint : MonoBehaviour
{
    Material weakPointMaterial;
    void Start()
    {
        weakPointMaterial = GetComponent<MeshRenderer>().material;
    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.transform.tag == "Proyectile")
        {
            weakPointMaterial.color = new Color(255, 0, 0); 
        }
    }
    
   
}
