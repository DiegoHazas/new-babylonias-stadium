﻿ using UnityEngine;
 using System.Collections;
 using UnityEngine.UI;
 
 public class FPSdisplay : MonoBehaviour
{
    Text fpsText;
    private float deltaTime;

    private void Start()
    {
        fpsText = GetComponent<Text>();
    }
    void Update()
    {
        deltaTime += (Time.deltaTime - deltaTime) * 0.1f;
        float fps = 1.0f / deltaTime;
        fpsText.text = Mathf.Ceil(fps).ToString();
    }
}