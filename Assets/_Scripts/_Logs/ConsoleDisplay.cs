﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ConsoleDisplay : MonoBehaviour
{
    public bool ConsoleActivated;
    Text ConsoleText;

    
    void Start()
    {
        ConsoleText = GetComponent<Text>();

        if (!ConsoleActivated)
        {
            ConsoleText.enabled = false;
           
        }
    }

    public void InserteConsole(string text)
    {
        if (!ConsoleActivated)
        {
            Debug.Log("Trying to InsertConsole but It is not activated");
        }
        else
        {
            ConsoleText.text = text;
        }
  
       
    }


}
