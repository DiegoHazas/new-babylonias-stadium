using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Serialization;

[RequireComponent(typeof(Animator))]

public class _GunFather : MonoBehaviour
{
    [SerializeField]
    protected LayerMask lmTarget;

    protected RaycastHit hit;

    public GameObject bulletType1, bulletType2;

    protected _PlayerInputs pInput;
    protected _PlayerInputs.AttacksActions shooting;

    protected _ObjectPools pool;

    protected _Player player;

    protected MeshRenderer mesh;

    protected Animator anim;

    [SerializeField]
    protected float secondaryShotCD, bpmCadency, maxSpread;

    public float reloadTime;

    protected float cadencyTimer, currentSpread, spreadAngle;

    [SerializeField]
    protected int clipSize;

    protected int currentAmmo;

    protected Vector3 currentForward;

    [SerializeField]
    protected Transform gt;

    protected Transform currentOrigin;

    [SerializeField]
    protected bool receiveInputs, hasSpecialEffect, automatic;

    protected bool reloading, inSemiCD, inCD, autoInputValue;
    

    protected void Reset()
    {
        if (!GetComponent<Animator>())
        {
            gameObject.AddComponent<Animator>();
        }

        else
        {
            anim = GetComponent<Animator>();
        }
    }

    protected virtual void Awake()
    {
        pInput = new _PlayerInputs();
        shooting = pInput.Attacks;
        mesh = GetComponent<MeshRenderer>();
        pool = FindObjectOfType<_ObjectPools>();

        if (receiveInputs)
        {
            if (!automatic)
            {
                shooting.PrimaryShoot.started += ctx => PrimaryShoot();
            }

            else
            {
                shooting.PrimaryShoot.started += ctx => autoInputValue = ctx.ReadValueAsButton();
                shooting.PrimaryShoot.canceled += ctx => autoInputValue = ctx.ReadValueAsButton();
            }
            
            shooting.SecondaryShoot.started += ctx => SecondaryShoot();
        }
    }

    protected virtual void OnEnable()
    {
        pInput.Enable();

        player = FindObjectOfType<_Player>();

        if (receiveInputs)
        {
            currentOrigin = player.cm.transform;
        }

        else
        {
            currentOrigin = transform.parent.transform;
        }
    }

    protected virtual void Start()
    {
        cadencyTimer = 0;
        currentAmmo = clipSize;
        currentSpread = 0;
        CalculateSpread();
    }

    protected virtual void Update()
    {
        //Aim();

        if (automatic)
        {
            if (!reloading)
            {
                cadencyTimer -= Time.deltaTime;

                if (autoInputValue)
                {
                    if (cadencyTimer < 0)
                    {
                        PrimaryShoot();
                        ResetTriggerCadency();
                    }
                }
            }
        }
    }

    protected virtual void OnDisable()
    {
        pInput.Disable();
    }

    public virtual void WeaponOut()
    {
        gameObject.SetActive(true);
    }

    public virtual void WeaponIn()
    {
        StopCoroutine(Reload(reloadTime));
        currentSpread = 0;
        CalculateSpread();
        reloading = false;
        gameObject.SetActive(false);
    }

    protected virtual void PrimaryShoot()
    {
        currentForward = player.cm.transform.forward;
        Aim();

        if (!inSemiCD && !reloading)
        {
            if (currentAmmo > 0)
            {
                _ProyectileFather proyec = pool.GrabFromPool(bulletType1.name, gt.position, gt.rotation).GetComponent<_ProyectileFather>();
                proyec.GetComponent<Rigidbody>().AddForce(gt.forward * proyec.speed, ForceMode.Impulse);

                currentAmmo -= 1;

                StartCoroutine(SemiCD());
            }

            else
            {
                StartCoroutine(Reload(reloadTime));
            }
        }
    }

    protected void ResetTriggerCadency()
    {
        cadencyTimer = 60 / bpmCadency;
    }

    protected virtual void SecondaryShoot()
    {
        if (!inCD)
        {
            _ProyectileFather proyec = pool.GrabFromPool(bulletType2.name, gt.position, gt.rotation).GetComponent<_ProyectileFather>();
            proyec.GetComponent<Rigidbody>().AddForce(gt.forward * proyec.speed, ForceMode.Impulse);

            if (hasSpecialEffect)
            {
                SpecialEffect();
            }

            StartCoroutine(SecondaryCD(secondaryShotCD));
        }
    }

    protected virtual void SpecialEffect()
    {
        
    }

    protected virtual void Aim()
    {
        if (Physics.Raycast(currentOrigin.position, currentForward, out hit, 200, lmTarget))
        {
            gt.LookAt(hit.point);
        }

        else
        {
            gt.LookAt(currentOrigin.position + currentForward * 200);
        }
    }

    protected virtual void CalculateSpread()
    {
        currentForward = Vector3.forward;
        currentSpread = Random.Range(0, maxSpread);
        spreadAngle = Random.Range(0, 360f);
        currentForward = Quaternion.AngleAxis(currentSpread, Vector3.up) * currentForward;
        currentForward = Quaternion.AngleAxis(spreadAngle, Vector3.forward) * currentForward;
        currentForward = player.cm.transform.rotation * currentForward;
    }

    protected virtual IEnumerator SemiCD()
    {
        inSemiCD = true;

        yield return new WaitForSeconds(60 / bpmCadency);

        inSemiCD = false;
    }

    public IEnumerator Reload(float time)
    {
        if (currentAmmo < clipSize && !reloading)
        {
            reloading = true;

            yield return new WaitForSeconds(time);

            currentAmmo = clipSize;
            currentSpread = 0;
            CalculateSpread();
            reloading = false;
        }

        else
        {
            yield return null;
        }
    }

    protected virtual IEnumerator SecondaryCD(float time)
    {
        inCD = true;

        yield return new WaitForSeconds(time);

        inCD = false;
    }
}