using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class _AssaultRifle : _GunFather
{
    [SerializeField]
    float cadencyBoost, slowMod, sightFOV, spreadDownSight;

    float normalFOV, originalCadency, originalSpread, originalSpeed, originalDashForce, originalJumpForce;

    Camera cm;

    bool onSights;

    protected override void Start()
    {
        base.Start();

        cm = player.cm.GetComponent<Camera>();
        normalFOV = cm.fieldOfView;
        originalSpread = maxSpread;
        originalCadency = bpmCadency;
        originalSpeed = player.speed;
        originalDashForce = player.dashForce;
        originalJumpForce = player.jumpForce;
    }

    protected override void SecondaryShoot()
    {
        if (!onSights)
        {
            cm.fieldOfView = sightFOV;
            mesh.enabled = false;
            bpmCadency *= cadencyBoost;
            player.speed *= slowMod;
            player.dashForce *= slowMod;
            player.jumpForce *= slowMod;
            maxSpread = spreadDownSight;
            onSights = true;
        }

        else
        {
            cm.fieldOfView = normalFOV;
            mesh.enabled = true;
            bpmCadency = originalCadency;
            player.speed = originalSpeed;
            player.dashForce = originalDashForce;
            player.jumpForce= originalJumpForce;
            maxSpread = originalSpread;
            onSights = false;
        }
    }

    public override void WeaponIn()
    {
        cm.fieldOfView = normalFOV;
        mesh.enabled = true;
        bpmCadency = originalCadency;
        player.speed = originalSpeed;
        player.dashForce = originalDashForce;
        player.jumpForce = originalJumpForce;
        maxSpread = originalSpread;
        onSights = false;

        base.WeaponIn();
    }
}
