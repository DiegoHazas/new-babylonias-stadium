using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;

public class _HandGun : _GunFather
{
    bool burst;

    protected override void Update()
    {
        Aim();

        if (burst && !inSemiCD)
        {
            if (currentAmmo > 0)
            {
                CalculateSpread();

                _ProyectileFather proyec = pool.GrabFromPool(bulletType1.name, gt.position, gt.rotation).GetComponent<_ProyectileFather>();
                proyec.GetComponent<Rigidbody>().AddForce(gt.forward * proyec.speed, ForceMode.Impulse);

                currentAmmo -= 1;

                StartCoroutine(SemiCD());
            }

            else
            {
                burst = false;
                currentSpread = 0;
                bpmCadency /= 4;
                StartCoroutine(Reload(reloadTime));
            }
        }
    }

    protected override void PrimaryShoot()
    {
        if (!burst)
        {
            base.PrimaryShoot();
        }
    }

    protected override void SecondaryShoot()
    {
        if (!reloading && !burst)
        {
            burst = true;
            bpmCadency *= 4;
        }
    }

    protected override IEnumerator SemiCD()
    {
        inSemiCD = true;

        yield return new WaitForSeconds(60 / bpmCadency);

        inSemiCD = false;
    }
}
