﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AimHeadEnemy : MonoBehaviour
{
    [SerializeField ]Transform Target;
    [SerializeField] Vector3 Offset;

    Animator anim;
    [SerializeField] Transform Head;
    void Start()
    {
        Target = FindObjectOfType<_Player>().transform;
        anim = GetComponent<Animator>();
        Head = anim.GetBoneTransform(HumanBodyBones.Head);
    }

    // Update is called once per frame
    void LateUpdate()
    {
        Head.LookAt(Target.position);
        Head.rotation = Head.rotation * Quaternion.Euler(Offset);
    }
}
