using System.Collections;
using System.Collections.Generic;
using System.Diagnostics.Tracing;
using TMPro;
using UnityEditor;
using UnityEditor.UIElements;
using UnityEngine;
using UnityEngine.SearchService;

public class _ProyectileFather : MonoBehaviour
{
    protected Rigidbody rb;

    [SerializeField]
    protected LayerMask ground;

    [SerializeField]
    protected string targetTag;

    [SerializeField]
    protected float lifeSpan;

    public float speed;

    protected virtual void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag(targetTag) || other.gameObject.layer == ground)
        {
            Hit();
        }
    }

    protected virtual void Awake()
    {
        rb = GetComponent<Rigidbody>();
    }

    protected virtual void Start()
    {
        StartCoroutine(LifeTimer());
    }

    protected virtual void Hit()
    {
        rb.velocity = Vector3.zero;
        gameObject.SetActive(false);
    }


    protected virtual IEnumerator LifeTimer()
    {
        yield return new WaitForSeconds(lifeSpan);

        Hit();
    }
}
