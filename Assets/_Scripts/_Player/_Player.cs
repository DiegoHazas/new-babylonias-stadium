using System.Collections;
using System.Collections.Generic;
using System.Security;
using TMPro;
using UnityEngine;
using UnityEngine.InputSystem;

public class _Player : MonoBehaviour
{
    Rigidbody rb;
    _PlayerInputs pInput;
    _PlayerInputs.PlayerActions movement;

    [SerializeField]
    float aerialMod, sens, dashCD, aerialDrag, groundDrag;

    public float speed, jumpForce, dashForce;

    float xRotation;

    Vector3 currentInput;

    [SerializeField]
    Vector3 newGravity;

    Transform weaponSlot1, weaponSlot2;

    [SerializeField]
    GameObject weapon1Prefab, weapon2Prefab;

    public GameObject cm;

    _GunFather weapon1, weapon2;

    [SerializeField]
    LayerMask ground;

    Vector2 movementInput, aimInput;

    bool doubleJump, grounded, canDash = true;

    private void Awake()
    {
        rb = GetComponent<Rigidbody>();
        cm = GetComponentInChildren<Camera>().gameObject;

        pInput = new _PlayerInputs();
        movement = pInput.Player;

        weaponSlot1 = GameObject.Find("Slot1").transform;
        weaponSlot2 = GameObject.Find("Slot2").transform;

        movement.Move.performed += ctx => movementInput = ctx.ReadValue<Vector2>();
        movement.Move.canceled += ctx => movementInput = ctx.ReadValue<Vector2>();

        movement.Look.performed += ctx => aimInput = ctx.ReadValue<Vector2>();
        movement.Look.canceled += ctx => aimInput = ctx.ReadValue<Vector2>();

        movement.Jump.started += ctx => Jump();

        movement.Dash.started += ctx => Dash();

        movement.Weapon1.started += ctx => SwitchToWeapon1();
        movement.Weapon2.started += ctx => SwitchToWeapon2();

        movement.Reload.started += ctx => Reload();
    }

    private void OnEnable()
    {
        pInput.Enable();
    }

    private void Start()
    {
        Cursor.visible = false;
        Cursor.lockState = CursorLockMode.Locked;
        Physics.gravity = newGravity;
        rb.drag = groundDrag;

        if (weapon1Prefab)
        {
            weapon1 = Instantiate(weapon1Prefab, weaponSlot1).GetComponent<_GunFather>();
        }

        if (weapon2Prefab)
        {
            weapon2 = Instantiate(weapon2Prefab, weaponSlot2).GetComponent<_GunFather>();
            weapon2.gameObject.SetActive(false);
        }
    }

    private void Update()
    {
        View();
    }

    private void FixedUpdate()
    {
        grounded = Physics.Raycast(transform.position, -transform.up, 1.1f, ground);

        Movement();
    }

    private void OnDisable()
    {
        pInput.Disable();
    }
    
    public void Movement()
    {
        currentInput = transform.forward * movementInput.y + transform.right * movementInput.x;

        if (grounded)
        {
            rb.AddForce(currentInput * speed);
            rb.useGravity = false;
            rb.drag = groundDrag;
        }

        else
        {
            rb.AddForce(currentInput * speed * aerialMod);
            rb.useGravity = true;
            rb.drag = aerialDrag;
        }
    }

    public void View()
    {
        float mouseX = aimInput.x * sens * Time.deltaTime;
        float mouseY = aimInput.y * sens * Time.deltaTime;

        xRotation -= mouseY;
        xRotation = Mathf.Clamp(xRotation, -90f, 90f);

        transform.Rotate(Vector3.up * mouseX);

        cm.transform.localRotation = Quaternion.Euler(xRotation, 0f, 0f);
    }

    public void Jump()
    {
        if (grounded)
        {
            rb.AddForce(transform.up * jumpForce + transform.forward * movementInput.y * jumpForce * 0.1f + transform.right * movementInput.x * jumpForce * 0.1f, ForceMode.Impulse);
            doubleJump = true;
        }

        else if (doubleJump)
        {
            rb.velocity = new Vector3(rb.velocity.x, 0, rb.velocity.z);
            rb.AddForce(transform.up * jumpForce * 0.7f + transform.forward * movementInput.y * jumpForce * 0.4f + transform.right * movementInput.x * jumpForce * 0.4f, ForceMode.Impulse);
            doubleJump = false;
        }
    }

    public void Dash()
    {
        if (currentInput != Vector3.zero && canDash)
        {
            if (grounded)
            {
                rb.AddForce(currentInput * dashForce, ForceMode.Impulse);
            }

            else
            {
                rb.AddForce(currentInput * dashForce * (aerialDrag / groundDrag), ForceMode.Impulse);
            }

            StartCoroutine(DashCD());
        }
    }

    void SwitchToWeapon1()
    {
        weapon1.WeaponOut();
        weapon2.WeaponIn();
    }

    void SwitchToWeapon2()
    {
        weapon1.WeaponIn();
        weapon2.WeaponOut();
    }

    void Reload()
    {
        if (weapon1.gameObject.activeInHierarchy)
        {
            StartCoroutine(weapon1.Reload(weapon1.reloadTime));
        }

        else if (weapon2.gameObject.activeInHierarchy)
        {
            StartCoroutine(weapon2.Reload(weapon2.reloadTime));
        }
    }

    IEnumerator DashCD()
    {
        canDash = false;

        yield return new WaitForSeconds(dashCD);

        canDash = true;
    }
}
