﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class _ObjectPools : MonoBehaviour
{
    [System.Serializable]

    public class Pool
    {
        public string name;
        public GameObject go;
        public int amount;

        public Pool(string nameC, GameObject goC, int amountC)
        {
            name = nameC;
            go = goC;
            amount = amountC;
        }
    }

    _GunFather[] guns;

    public Dictionary<string, Queue<GameObject>> dictionaryPools;
    public List<Pool> l_pools;

    private void Start()
    {
        guns = FindObjectsOfType<_GunFather>();
        dictionaryPools = new Dictionary<string, Queue<GameObject>>();

        if (guns.Length > 0)
        {
            for (int i = 0; i < guns.Length; i++)
            {
                Pool pool1 = new Pool(guns[i].bulletType1.name, guns[i].bulletType1, 50);
                Pool pool2 = new Pool(guns[i].bulletType2.name, guns[i].bulletType2, 50);

                l_pools.Add(pool1);
                l_pools.Add(pool2);
            }
        }

        foreach (Pool pool in l_pools)
        {
            Queue<GameObject> goPool = new Queue<GameObject>();

            for (int i = 0; i < pool.amount; i++)
            {
                GameObject obj = Instantiate(pool.go);
                obj.SetActive(false);
                goPool.Enqueue(obj);
            }

            if (!dictionaryPools.ContainsKey(pool.name))
            {
                dictionaryPools.Add(pool.name, goPool);
            }
        }
    }

    public GameObject GrabFromPool(string name, Vector3 position, Quaternion rotation)
    {
        if (!dictionaryPools.ContainsKey(name))
        {
            return null;
        }

        GameObject spawnedGO = dictionaryPools[name].Dequeue();

        spawnedGO.SetActive(true);
        spawnedGO.transform.position = position;
        spawnedGO.transform.rotation = rotation;

        dictionaryPools[name].Enqueue(spawnedGO);

        return spawnedGO;
    }
}
