using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class _GraphicStuff : MonoBehaviour
{
    [SerializeField]
    int fps;

    void Start()
    {
        Application.targetFrameRate = fps;
    }
}
